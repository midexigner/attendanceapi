const mongoose = require('mongoose'),Schema = mongoose.Schema;

const departmentSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    user_id: [{ type: Schema.Types.ObjectId, ref: 'Users' }],
    status:{
        type:String,
        default: 'active'
    }
},{
    timestamps: true
});

const Department = mongoose.model('department', departmentSchema);

module.exports = Department;