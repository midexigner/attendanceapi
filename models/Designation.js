const mongoose = require('mongoose'),Schema = mongoose.Schema;

const designationSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    department_id: [{ type: Schema.Types.ObjectId, ref: 'Department' }],
    user_id: [{ type: Schema.Types.ObjectId, ref: 'Users' }],
    status:{
        type:String,
        default: 'active'
    }
},{
    timestamps: true
});

const Designation = mongoose.model('designation', designationSchema);

module.exports = Designation;