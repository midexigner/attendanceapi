const mongoose = require('mongoose'),Schema = mongoose.Schema;

const attendancesSchema = new mongoose.Schema({
    check_in: {
        type: Boolean,
    },
    check_in_time:{
        type: Date,
    },
    check_out: {
        type: String
    },
    check_out_time:{
        type: Date,
    },
    ip_address: {
        type: String
    },
    user_id: [{ type: Schema.Types.ObjectId, ref: 'Users' }],
},{
    timestamps: true
});

const Attendances = mongoose.model('Attendances', attendancesSchema);

module.exports = Attendances;

