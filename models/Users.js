const mongoose = require("mongoose");
const { isEmail } = require("validator");
const bcrypt = require("bcrypt");

const userSchema = new mongoose.Schema(
  {
    FirstName: {
      type: String,
      required: true,
    },
    LastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: [true, "Please enter an email"],
      unique: true,
      lowercase: true,
      validate: [isEmail, "Please enter a valid email"],
    },
    password: {
      type: String,
      required: [true, "Please enter a password"],
      minlength: [6, "Minimum password length is 6 characters"],
    },
    gender: {
      type: String,
      required: true,
    },
    DOB: {
      type: Date,
      required: true,
    },
    DateOfJoining: {
      type: Date,
      required: true,
    },
    Avatar: {
      type: String,
      default:
        "https://res.cloudinary.com/mi-dexigner/image/upload/v1612555416/avatar_xotmal.png",
    },
    ContactNo: { type: String, required: true },
    EmployeeCode: { type: String, required: true },
    Account: { type: Number, required: true },
    TotalAssignLeave: { type: Number, required: true },
    Position: [{ type: mongoose.Schema.Types.ObjectId, ref: "Position" }],
    Department: [{ type: mongoose.Schema.Types.ObjectId, ref: "Department" }],
    // salary: [{ type: mongoose.Schema.Types.ObjectId, ref: "Salary" }],
    // education: [{ type: mongoose.Schema.Types.ObjectId, ref: "Education" }],
  
    // workExperience: [
    //   { type: mongoose.Schema.Types.ObjectId, ref: "WorkExperience" }
    // ],
    
    BloodGroup: { type: String },
    EmergencyContactNo: { type: String },
    nicNo: { type: String },
    PermanetAddress: { type: String },
    PresentAddress: { type: String },
    role: {
      type: String,
      default: "",
    },
  },
  {
    timestamps: true,
  }
);

// fire a function after doc saved to db
/*userSchema.post('save', function (doc, next) {
  console.log('new user was created & saved', doc);
  next();
});*/



// static method to login user
userSchema.statics.login = async function (email, password) {
  const user = await this.findOne({ email });
  if (user) {
    const auth = await bcrypt.compare(password, user.password);
    if (auth) {
      return user;
    }
    throw Error("incorrect password");
  }
  throw Error("incorrect email");
};

const User = mongoose.model("user", userSchema);

module.exports = User;
