const mongoose = require('mongoose'),Schema = mongoose.Schema;

const settingSchema = new mongoose.Schema({
    company_name: {
        type: String,
        required: true
    },
    address: {
        type: String
    },
    country: {
        type: String
    },
    city: {
        type: String
    },
    state: {
        type: String
    },
    postal_code: {
        type: String
    },
    email: {
        type: String
    },
    phone: {
        type: Number
    },
    mobile: {
        type: Number
    },
    fax: {
        type: Number
    },
    website_url: {
        type: String
    },
    user_id: [{ type: Schema.Types.ObjectId, ref: 'Users' }],
    
},{
    timestamps: true
});

const Settings = mongoose.model('settings', settingSchema);

module.exports = Settings;

settingSchema.path('website_url').validate((val) => {
    urlRegex = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-/]))?/;
    return urlRegex.test(val);
}, 'Invalid URL.');