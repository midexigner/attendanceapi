const express = require('express');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const http = require('http');
const cors = require('cors');
require("dotenv").config()


const app = express();
const httpServer = http.createServer(app);
const PORT = process.env.PORT || 8080;

//  middleware
app.use(cors());
app.use(express.json());
app.use(cookieParser());

// database connection start
const dbURI = process.env.DATABASE_URL;
mongoose.connect(dbURI, { 
  useCreateIndex:true,
  useNewUrlParser:true,
  useUnifiedTopology:true
}).then(() => console.warn("Conneted to the Database Successfull!"))
  .catch((err) => console.log(err));
// database connection end

// set the end points for the routes
const authRoutes = require('./routes/authRoutes');
const usersRoutes = require('./routes/usersRoutes');
const departmentRoutes = require('./routes/departmentRoutes');
const destinationRoutes = require('./routes/designationRoutes');
const settingsRoutes = require('./routes/SettingsRoutes');
const attendancesRoutes = require('./routes/AttendancesRoutes');


app.use('/api/auth', authRoutes)
app.use('/api/user', usersRoutes)
app.use('/api/department', departmentRoutes)
app.use('/api/designation', destinationRoutes)
app.use('/api/settings', settingsRoutes)
app.use('/api/attendances', attendancesRoutes)

app.get('/',(req,res)=>{
  //res.redirect('https://miattendance.vercel.app/')
  //res.send("working");
  res.send('<b>Hello</b> welcome to my http server made with express');
  console.log('HRMS')
}) 
// Change the 404 message modifing the middleware
app.use(function(req, res, next) {
  res.status(404).send("Sorry, that route doesn't exist. Have a nice day :)");
});

httpServer.listen(PORT, () =>
  console.log(`Backend server is running!: http://localhost:${PORT}`)
);