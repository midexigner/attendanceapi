const { Router } = require("express")
const departmentController = require('../controller/departmentController')

const router = Router();

router.get('/',departmentController.get);
router.get('/:id',departmentController.getById);
router.post('/',departmentController.post);
router.put('/:id',departmentController.update);
router.delete('/:id',departmentController.delete);

module.exports = router;