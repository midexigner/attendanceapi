const { Router } = require("express")
const authController = require('../controller/authController');
const router = Router();

router.post('/login',authController.login_post);
router.get('/logout', authController.logout_get);

module.exports = router;

