const { Router } = require("express")
const SettingsController = require('../controller/SettingsController');
const router = Router();

router.get('/',SettingsController.get);
router.get('/:id',SettingsController.getById);
router.post('/',SettingsController.post);
router.put('/:id',SettingsController.update);
router.delete('/:id',SettingsController.delete);

module.exports = router;