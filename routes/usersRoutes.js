const { Router } = require("express")
const usersController = require('../controller/usersController');
const router = Router();

router.get('/',usersController.user_get);
router.get('/:id',usersController.user_get_byID);
router.post('register',usersController.user_post);
router.put('/:id',usersController.user_update_byID);
router.delete('/:id',usersController.user_delete);

module.exports = router;