const { Router } = require("express")
const AttendancesController = require('../controller/AttendancesController')

const router = Router();

router.get('/',AttendancesController.get);
router.get('/:id',AttendancesController.getById);
router.post('/',AttendancesController.post);
router.put('/:id',AttendancesController.update);
router.delete('/:id',AttendancesController.delete);

module.exports = router;