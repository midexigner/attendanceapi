const { Router } = require("express")
const designationController = require('../controller/designationController')

const router = Router();
router.get('/',designationController.get);
router.get('/:id',designationController.getById);
router.post('/',designationController.post);
router.put('/:id',designationController.update);
router.delete('/:id',designationController.delete);

module.exports = router;