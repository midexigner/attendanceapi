const Designation = require('../models/Designation')


// destination listing
module.exports.get = async (req, res) => {
    await Designation.find()
    .then(results => res.json(results))
    .catch(err => res.status(400).json('Error: ' + err));  
}

// destination by id
module.exports.getById = async (req, res) => {
    const id = req.params.id;
    await Designation.findById(id)
    .then(result => res.json(result))
    .catch(err => res.status(400).json('Error: ' + err));
}

// destination create
module.exports.post = async (req, res) => {
    const {name,description,department_id ,user_id,status} = req.body;
    try {
    const designation = await Designation.create({name,description,department_id, user_id,status});
        res.status(201).json({ designation: designation._id });
    }
    catch(err) {
        res.status(400).json({ err });
      }
}

// destination update
module.exports.update = async (req, res) => {
    const id = req.params.id;
    const {name,description, user_id,status} = req.body;

    await Destination.updateOne(
        {_id:id},
        { $set: { name: name, description: description,department_id: department_id ,user_id: user_id,status: status} }
        ).then(()=> res.json({message:"Destination Update"}))
        .catch(err => res.status(400).json('Error: ' + err))

}

// destination delete
module.exports.delete = async (req, res) => {
    const id = req.params.id;
    await Destination.remove({_id:id})
    .then(()=> res.json({message:"Destination Delete"}))
    .catch(err => res.status(400).json('Error: ' + err))
}