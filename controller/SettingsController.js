const Settings = require('../models/Settings')


// settings listing
module.exports.get = async (req, res) => {
    await Settings.find()
    .then(results => res.json(results))
    .catch(err => res.status(400).json('Error: ' + err));
  }


// settings by id
module.exports.getById = async (req, res) => {
  try {
    const post = await Settings.findById(req.params.id);
    res.status(200).json(post);
  } catch (err) {
    res.status(500).json(err);
  }
}

// settings create
module.exports.post = async (req, res) => {
    const newPost = new Settings(req.body);
  try {
    const savedPost = await newPost.save();
    res.status(200).json(savedPost);
  } catch (err) {
    res.status(500).json(err);
  }
}

// settings update
module.exports.update = async (req, res) => {
    try {
        const settings = await Settings.findById(req.params.id);
        if (settings.user_id[0] == req.body.user_id) {
          try {
            const updatedSettings = await Settings.findByIdAndUpdate(
              req.params.id,
              {
                $set: req.body,
              },
              { new: true }
            );
            res.status(200).json(updatedSettings);
          } catch (err) {
            res.status(500).json(err);
          }
        } else {
          res.status(401).json("You cannot update the Settings!");
        }
      } catch (err) {
        res.status(500).json(err);
      }
}

// settings delete
module.exports.delete = async (req, res) => {}