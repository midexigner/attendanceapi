const User = require("../models/Users");
const jwt = require('jsonwebtoken');

const handleErrors = (err) => {
  console.log(err.message, err.code);
  let errors = { email: '', password: '' };

  // incorrect email
  if (err.message === 'incorrect email') {
    errors.email = 'That email is not registered';
  }

  // incorrect password
  if (err.message === 'incorrect password') {
    errors.password = 'That password is incorrect';
  }

  // duplicate email error
  if (err.code === 11000) {
    errors.email = 'that email is already registered';
    return errors;
  }

  // validation errors
  if (err.message.includes('user validation failed')) {
    // console.log(err);
    Object.values(err.errors).forEach(({ properties }) => {
      // console.log(val);
      // console.log(properties);
      errors[properties.path] = properties.message;
    });
  }

  return errors;
}
// create json web token
const maxAge = 3 * 24 * 60 * 60;
const createToken = (id) => {
  return jwt.sign({ id }, '3z0xHPVxCjyUx3qhf5SFUIGCNAEzfil8', {
    expiresIn: maxAge
  });
};

// users listing
  module.exports.user_get = async (req, res) => {
    await User.find()
        .then(users => res.json(users))
        .catch(err => res.status(400).json('Error: ' + err));
    }

  // users register
  module.exports.user_post = async (req, res) => {
    const {name,email, password,avatar,role} = req.body;
  
    try {
      const user = await User.create({name,email, password,avatar,role});
      const token = createToken(user._id);
      res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000 });
      res.status(201).json({ user: user._id });
    }
    catch(err) {
      const errors = handleErrors(err);
      res.status(400).json({ errors });
    }
   
  }

  //user by id
  module.exports.user_get_byID = async (req, res) => {
    const id = req.params.id;
      await User.findById(id)
    .then(user => res.json(user))
    .catch(err => res.status(400).json('Error: ' + err));
}
//  user update
module.exports.user_update_byID = async (req,res) =>{
  const id = req.params.id;
  const {name,email, password,avatar,role} = req.body;
  await User.updateOne(
    {_id:id},
    { $set: { name: name, email: email,password: password,avatar: avatar, role: role} }
    ).then(()=> res.json({message:"User Update"}))
    .catch(err => res.status(400).json('Error: ' + err))
}

// user delete
module.exports.user_delete =  async (req,res) =>{
  const id = req.params.id;
  await User.remove({_id:id})
  .then(()=> res.json({message:"User Delete"}))
  .catch(err => res.status(400).json('Error: ' + err))
}