const Department = require('../models/Department')

// department listing
module.exports.get = async (req, res) => {
    await Department.find()
        .then(results => res.json(results))
        .catch(err => res.status(400).json('Error: ' + err));
 }
 // department by id
 module.exports.getById = async (req, res) => {
    const id = req.params.id;
    await Department.findById(id)
    .then(result => res.json(result))
    .catch(err => res.status(400).json('Error: ' + err));
 }

 // department create
 module.exports.post = async (req, res) => {
    const {name,description, user_id,status} = req.body;
    try {
    const department = await Department.create({name,description, user_id,status});
   // Department.watch().on("change",data => console.log(new Date(), data));
    res.status(201).json({ department: department._id });
    }
    catch(err) {
        res.status(400).json({ err });
      }
 }

 // department update
 module.exports.update = async (req, res) => {
    const id = req.params.id;
    const {name,description, user_id,status} = req.body;

    await Department.updateOne(
        {_id:id},
        { $set: { name: name, description: description,user_id: user_id,status: status} }
        ).then(()=> res.json({message:"Department Update"}))
        .catch(err => res.status(400).json('Error: ' + err))
 }

 // department delete
 module.exports.delete = async (req, res) => {
    const id = req.params.id;
    await Department.deleteOne({_id:id})
    .then(()=> res.json({message:"Department Delete"}))
    .catch(err => res.status(400).json('Error: ' + err))
 }