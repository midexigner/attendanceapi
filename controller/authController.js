const User = require("../models/Users");
const jwt = require('jsonwebtoken');
const {handleErrors} = require("../helpers/errorHandling")

// create json web token
const maxAge = 3 * 24 * 60 * 60;
const createToken = (id) => {
  return jwt.sign({ id }, '3z0xHPVxCjyUx3qhf5SFUIGCNAEzfil8', {
    expiresIn: maxAge
  });
};
// controller actions
  module.exports.login_post = async (req, res) => {
    const { email, password } = req.body;
  
    try {
      const user = await User.login(email, password);
      const token = createToken(user._id);
      res.cookie('jwt', token, { httpOnly: true, maxAge: maxAge * 1000 });
      //console.log(user);
      res.status(200).json({ user: user,token:token});
    } 
    catch (err) {
      const errors = handleErrors(err);
      res.json({ errors });
    }
  
  }

  module.exports.logout_get = (req, res) => {
    res.cookie('jwt', '', { maxAge: 1 });
    res.redirect('/');
  }